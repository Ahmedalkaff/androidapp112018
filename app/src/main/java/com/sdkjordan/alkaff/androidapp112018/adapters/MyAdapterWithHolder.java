package com.sdkjordan.alkaff.androidapp112018.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sdkjordan.alkaff.androidapp112018.R;

import java.util.LinkedList;

public class MyAdapterWithHolder extends BaseAdapter implements View.OnLongClickListener {
    private LinkedList<Contact> data;
    LayoutInflater inflater ;
    private Context mContext;

    public MyAdapterWithHolder(Context context) {
        mContext = context;
        data = new LinkedList<>();
        inflater = LayoutInflater.from(mContext);
    }

    public void addContact(String name, String phone, int imageId) {
        Contact contact = new Contact(name, phone, imageId);
        data.add(contact);
        notifyDataSetChanged();
    }

    public boolean removeContact(int position) {
        if (position < 0 || position > data.size())
            throw new IndexOutOfBoundsException();

        data.remove(position);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Contact getItem(int position) {
        if (position < 0 || position >= data.size())
            throw new IndexOutOfBoundsException();

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean onLongClick(View v) {

        return false;
    }


    private static class ViewHolder {
        TextView TVName;
        TextView TVPhone;
        ImageView IVAvatar;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Contact currentContact = getItem(position);

        ViewHolder myViewHolder;

//        final View result;

        if (convertView == null) {
            myViewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.contact_item_layout, parent, false);
            myViewHolder.TVName = convertView.findViewById(R.id.textViewContactName);
            myViewHolder.TVPhone = convertView.findViewById(R.id.textViewPhoneNumber);
            myViewHolder.IVAvatar = convertView.findViewById(R.id.imageViewAvatar);

//            result = convertView;
            convertView.setTag(myViewHolder);
        } else {
            myViewHolder = (ViewHolder) convertView.getTag();
//            result = convertView;
        }

        myViewHolder.TVName.setText(currentContact.getName());
        myViewHolder.TVPhone.setText(currentContact.getPhone());
        switch (currentContact.getImage()) {
            case 1:
                myViewHolder.IVAvatar.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.raw.avatar1));
                break;
            case 2:
                myViewHolder.IVAvatar.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.raw.avatar2));
                break;
        }
        myViewHolder.IVAvatar.setOnLongClickListener(this);

        // Return the completed view to render on screen
        return convertView;

    }


}
