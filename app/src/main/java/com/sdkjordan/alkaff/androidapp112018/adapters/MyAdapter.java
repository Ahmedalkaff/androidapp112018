package com.sdkjordan.alkaff.androidapp112018.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;
import com.sdkjordan.alkaff.androidapp112018.R;

import java.util.HashMap;
import java.util.LinkedList;

public class MyAdapter extends BaseAdapter {
    private LinkedList<HashMap<String, String>> data;

    private Context mContext;

    public MyAdapter(Context context) {
        mContext = context;
        data = new LinkedList<>();
    }

    public void addContact(String name, String phone, int imageId) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.NAME_KEY, name);
        map.put(Constants.PHONE_KEY, phone);
        map.put(Constants.IMAGE_KEY, String.valueOf(imageId));

        data.add(map);
        notifyDataSetChanged();
    }

    public boolean removeContact(int position) {
        if (position < 0 || position > data.size())
            throw new IndexOutOfBoundsException();

        data.remove(position);
        return true;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        if (position < 0 || position >= data.size())
            throw new IndexOutOfBoundsException();

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        if(convertView == null)
//        {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View mvView = inflater.inflate(R.layout.contact_item_layout, null);

        TextView name = mvView.findViewById(R.id.textViewContactName);
        name.setText(getItem(position).get(Constants.NAME_KEY));
        TextView phone = mvView.findViewById(R.id.textViewPhoneNumber);
        phone.setText(getItem(position).get(Constants.PHONE_KEY));

        ImageView mImageView = mvView.findViewById(R.id.imageViewAvatar);
        switch (getItem(position).get(Constants.IMAGE_KEY)) {
            case "1":
                mImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.raw.avatar1));
                break;
            case "2":
                mImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.raw.avatar2));
                break;
        }

//        }
        return mvView;
    }
}
