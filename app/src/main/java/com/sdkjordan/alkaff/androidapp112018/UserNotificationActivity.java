package com.sdkjordan.alkaff.androidapp112018;

import android.app.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;
import org.w3c.dom.Notation;
import org.w3c.dom.Text;

import java.util.Calendar;

public class UserNotificationActivity extends AppCompatActivity implements DialogInterface.OnClickListener {


    /// Single choice list dialog variables
    private AlertDialog.Builder mSingleListbuilder;
    private AlertDialog mSingleListAlertDialog;
    private ArrayAdapter<String> mListArrayAdapter;
    private int SelectID = -1;

    /// Multi choice list dialog variables
    private AlertDialog.Builder mMultiListbuilder;
    private AlertDialog mMultiListAlertDialog;
    boolean[] mCheckedItems;

    /// Input text dialog variables
    private AlertDialog.Builder mInputbuilder;
    private AlertDialog mInputAlertDialog;
    EditText editText;


    ///  Text dialog variables
    private AlertDialog.Builder mTextbuilder;
    private AlertDialog mTextAlertDialog;


    private TextView mTextViewData;
    private String channelID = "channel_1";


    /// Notification variables

    Notification notification;
    NotificationCompat.Builder builder;
    NotificationManager mnNotificationManager;
    Intent mIntent;
    PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notification);

        mTextViewData = findViewById(R.id.textViewData);


        createNotificationChannel("Second", "description", "2");
        mnNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonToast:
                showToast("Toast");
                break;
            case R.id.buttonCustomToast:
                showCustomToast("This is a fully custom Toast");
                break;
            case R.id.buttonSingleListDialog:
                showSingleListDialog(R.array.Contents);
                break;
            case R.id.buttonMultiChoiceDialog:
                showMultiListDialog(R.array.Contents);
                break;
            case R.id.buttonTextDialog:
                showTextDialog("This is a text dialog");
                break;
            case R.id.buttonInputDialog:
                showInputDialog("Name");
                break;
            case R.id.buttonNotification:
                showNotification("This is the message");
                break;
        }
    }

    private void showNotification(String msg) {
        if (builder == null) {
            mIntent = new Intent(Constants.START_ACTION);
            pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Action action_OK = new NotificationCompat.Action(R.drawable.ic_face, "OK", pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (mnNotificationManager.getNotificationChannel(channelID) == null)
                    createNotificationChannel(channelID, getString(R.string.channel_description), getString(R.string.channel_name));

                builder = new NotificationCompat.Builder(getApplicationContext(), channelID);
            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                //builder.setTimeoutAfter(10000);
//        }


            Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.avatar1);


            builder.setContentTitle("Notification")
                    .setContentText(msg)
                    .setLargeIcon(img)
                    //.setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setSmallIcon(android.R.drawable.stat_notify_chat)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setColorized(true)
                    .setColor(getResources().getColor(R.color.colorPrimaryDark))
                    .addAction(action_OK)
                    .addAction(R.drawable.ic_notify, getString(R.string.notify),
                            pendingIntent)
                    .setTicker(Calendar.getInstance().getTime().toString());
        }

        notification = builder.build();

        // TO show the notification
        if (mnNotificationManager != null)
            mnNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // the notification will be shown
        mnNotificationManager.notify(Constants.NOTIFICATION_ID, notification);


    }

    private void createNotificationChannel(String chaID, String desc, String name) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = getString(R.string.channel_name);
//            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(chaID, name, importance);
            channel.setDescription(desc);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showInputDialog(String name) {

        if (mInputAlertDialog == null) {
            if (mInputbuilder == null) {
                mInputbuilder = new AlertDialog.Builder(this);
                View view = getLayoutInflater().inflate(R.layout.input_dialog_layout, null);
                editText = view.findViewById(R.id.editTextInput);
                editText.setHint(name);
                mInputbuilder.setTitle(R.string.txt_contents)
                        .setCancelable(false)
                        .setTitle("Enter your " + name)
                        .setView(view)
                        .setNegativeButton(R.string.txt_cancel, this)          // or as in the next lines
//                        .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        })
                        .setPositiveButton(R.string.txt_ok, this)          // or as in the next lines
//                        .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                mTextViewData.setText(editText.getText());
//                            }
//                        })
                ;
            }
            mInputAlertDialog = mInputbuilder.create();
        }

        mInputAlertDialog.show();
    }


    private void showSingleListDialog(final int data) {
        if (mSingleListAlertDialog == null) {
            if (mSingleListbuilder == null) {
                mSingleListbuilder = new AlertDialog.Builder(this);
                if (mListArrayAdapter == null)
                    mListArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_single_choice, getResources().getStringArray(data));
                mSingleListbuilder.setTitle(R.string.txt_contents)
                        .setCancelable(false)
                        .setSingleChoiceItems(mListArrayAdapter, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SelectID = which;
                            }
                        })
                        // or you can
//                        .setSingleChoiceItems(data,-1,new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                SelectID = which;
//                            }
//                        })
                        .setPositiveButton(R.string.txt_cancel, this)          // or as in the next lines
//                        .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        })
                        .setPositiveButton(R.string.txt_ok, this)          // or as in the next lines
//                        .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                if (SelectID >= 0) {
//                                    mTextViewData.setText(getResources().getStringArray(data)[SelectID]);
//                                } else {
//                                    mTextViewData.setText("No thing is selected");
//                                }
//                            }
//                        })
                ;

            }
            mSingleListAlertDialog = mSingleListbuilder.create();
        }

        mSingleListAlertDialog.show();
    }

    private void showMultiListDialog(final int data) {

        mCheckedItems = new boolean[getResources().getStringArray(data).length];
        mCheckedItems[0] = true;

        if (mMultiListAlertDialog == null) {
            if (mMultiListbuilder == null) {
                mMultiListbuilder = new AlertDialog.Builder(this);
                mMultiListbuilder.setTitle(R.string.txt_contents)
                        .setCancelable(false)
                        .setMultiChoiceItems(R.array.Contents, mCheckedItems, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                mCheckedItems[which] = isChecked;
                            }
                        })
                        .setNegativeButton(R.string.txt_cancel, this)          // or as in the next lines
//                        .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        })
                        .setPositiveButton(R.string.txt_ok, this)          // or as in the next lines
//                        .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                String[] contents = getResources().getStringArray(data);
//                                StringBuffer buffer = new StringBuffer();
//                                for (int i = 0; i < mCheckedItems.length; i++) {
//                                    if (mCheckedItems[i])
//                                        buffer.append(contents[i]).append(", ");
//                                }
//
//                                mTextViewData.setText(buffer.toString());
//                            }
//                        })
                ;

            }
            mMultiListAlertDialog = mMultiListbuilder.create();
        }

        mMultiListAlertDialog.show();
    }


    private void showTextDialog(String text) {
        if (mTextAlertDialog == null) {
            if (mTextbuilder == null) {
                mTextbuilder = new AlertDialog.Builder(this);
                mTextbuilder.setTitle("Text dialog")
                        .setCancelable(false)
                        .setTitle(text)
                        .setPositiveButton(R.string.txt_cancel, this)
                        .setPositiveButton(R.string.txt_ok, this);
            }
            mTextAlertDialog = mTextbuilder.create();
        }
        mTextAlertDialog.show();


    }

    private void showCustomToast(String text) {
        Toast toast = new Toast(getApplicationContext());
        View view = getLayoutInflater().inflate(R.layout.custom_toast, null);
        ImageView imageView = view.findViewById(R.id.customToastImage);         // you can skip this if you don't want to change anything in the image

        TextView textView = view.findViewById(R.id.customToastText);
        textView.setText(text);

        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 50);
        toast.show();

    }

    private void showToast(String text) {
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 50);
        toast.show();
    }


    /**
     * This method to handle all the three possible dialog button clicks for all the dialogs at the same time
     *
     * @param dialog
     * @param which
     */
    @Override
    public void onClick(DialogInterface dialog, int which) {
        // dismiss any dialog if cancel is clicked
        if (which == DialogInterface.BUTTON_NEGATIVE)
            dialog.dismiss();

        if (dialog == mInputAlertDialog) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    mTextViewData.setText(editText.getText());
                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    // TODO: handle the nutrual button click if exist
                    break;
            }
        } else if (dialog == mSingleListAlertDialog) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    if (SelectID >= 0)
                        mTextViewData.setText(getResources().getStringArray(R.array.Contents)[SelectID]);
                    else
                        mTextViewData.setText("No thing is selected");

                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    // TODO: handle the nutrual button click if exist
                    break;
            }
        } else if (dialog == mMultiListAlertDialog) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    String[] contents = getResources().getStringArray(R.array.Contents);
                    StringBuffer buffer = new StringBuffer();
                    for (int i = 0; i < mCheckedItems.length; i++)
                        if (mCheckedItems[i])
                            buffer.append(contents[i]).append(", ");

                    mTextViewData.setText(buffer.toString());

                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    // TODO: handle the nutrual button click if exist
                    break;
            }
        }

        SelectID = which;
    }
}
