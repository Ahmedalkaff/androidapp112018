package com.sdkjordan.alkaff.androidapp112018.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

public class MyIntentService extends IntentService {
    private static final String TAG = "MyService";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MyIntentService(String name) {
        super(name);
    }

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, String.format("%s-->%20s %n", Thread.currentThread().getName(), intent.getStringExtra(Constants.EXTRA_DATA)));
    }
}
