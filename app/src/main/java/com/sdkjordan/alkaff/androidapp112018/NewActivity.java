package com.sdkjordan.alkaff.androidapp112018;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

public class NewActivity  extends AppCompatActivity  implements View.OnClickListener {

    private static final String TAG = "NewActivity";
    EditText mEditText, mEditTextPhone, mEditTextSite;
    Button mButtonFinish ;
    ImageButton mButtonCall , mButtonView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        Log.e(TAG,"NewActivity: onCreate");
        mButtonFinish = findViewById(R.id.buttonFinish);
        mButtonFinish.setOnClickListener(this);
        mEditTextPhone = findViewById(R.id.editTextPhone);
        mEditTextSite = findViewById(R.id.editTextSite);
        mEditText = findViewById(R.id.editText2);
        mButtonCall = findViewById(R.id.imageButtonCall);
        mButtonCall.setOnClickListener(this);
        mButtonView = findViewById(R.id.imageButtonView);
        mButtonView.setOnClickListener(this);
        Intent intent = getIntent();
         if(intent != null)
         {
             String text = intent.getStringExtra(Constants.EXTRA_DATA);

             if(text != null)
                 mEditText.setText(text);
             else
                 mEditText.setText("No date!!");
         }

    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG,"NewActivity: onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG,"NewActivity: onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"NewActivity: onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG,"NewActivity: onPause");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"NewActivity: onResume");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG,"NewActivity: onRestart");

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.buttonFinish:
                String result = mEditText.getText().toString();
                if(result != null && result.length() > 0)
                {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.EXTRA_RESULT,result);
                    setResult(RESULT_OK,intent);
                }else {
                    setResult(RESULT_CANCELED);
                }
                finish();
                break;
            case R.id.imageButtonCall:
                makeTheCall(mEditTextPhone.getText().toString());
                break;
            case R.id.imageButtonView:
                viewSite(mEditTextSite.getText().toString());
                break;
        }
    }

    private void viewSite(String s) {
        if(s != null && s.length() > 3)
        {
            String link = s ;
            if(! s.startsWith("http"))
                link = "http://"+s;
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(link));
            startActivity(intent);
        }
    }

    private void makeTheCall(String s) {
        if(s != null && s.length() > 3)
        {
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("tel://"+s));
            startActivity(intent);
        }
    }
}
