package com.sdkjordan.alkaff.androidapp112018.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.DB.Contacts.TABLE_NAME;
import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.DB.Contacts.CONTACTS_ID;
import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.DB.Contacts.CONTACTS_NAME;
import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.DB.Contacts.CONTACTS_Phone;

public class DatabaseHelper extends SQLiteOpenHelper {

    ContentValues values;
    private static final String CreateContactTable = "CREATE TABLE " + TABLE_NAME + " (" +
            Constants.DB.Contacts.CONTACTS_ID + " INTEGER  PRIMARY KEY , " +
            Constants.DB.Contacts.CONTACTS_NAME + " TEXT NOT NULL, " +
            Constants.DB.Contacts.CONTACTS_Phone + " text NOT NULL UNIQUE" + ");";


    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context) {

        super(context, Constants.DB.DB_NAME, null, Constants.DB.VERSION);
        values = new ContentValues();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mDatabase = db;
        db.execSQL(CreateContactTable);

    }


    public long insertUser(String name, String phone) {
        int id = getNewContactID();

        values.clear();
        values.put(Constants.DB.Contacts.CONTACTS_ID, id);
        values.put(Constants.DB.Contacts.CONTACTS_NAME, name);
        values.put(Constants.DB.Contacts.CONTACTS_Phone, phone);
        return getWritableDatabase().insert(Constants.DB.Contacts.TABLE_NAME, null, values);
    }

    public Cursor getUsers() {
        return getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
    }

    public int changeUserPhone(String name, String phone) {
        values.clear();
        values.put(CONTACTS_Phone, phone);
        String where = CONTACTS_NAME + " = ? AND " + CONTACTS_Phone + " = ?";
        String[] args = {name, phone};
        return getReadableDatabase().update(TABLE_NAME, values, where, args);
    }

    public int changeUserPhone(int id, String phone) {
        values.clear();
        values.put(CONTACTS_Phone, phone);
        // To avoid SQl injection use ? and argument parameter arrays
        String where = CONTACTS_ID + " = ? AND " + CONTACTS_Phone + " = ?";
        String[] args = {String.valueOf(id), phone};
        return getReadableDatabase().update(TABLE_NAME, values, where, args);
    }


    public Cursor getUserInfo(String name, String password) {
        String selection = CONTACTS_NAME + " = ? AND " + CONTACTS_Phone + " = ?";
        String[] args = {name, password};
        return getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: it important to create a script that migrate from the old database to the new one if needed!
        // now that is outside out scope
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        // TODO: it important to create a script that migrate from the new database to the old one if needed!
        // now that is outside out scope
    }


    public static final String MAX_ID_SQL = "select max(" + Constants.DB.Contacts.CONTACTS_ID + ") as ID from " + Constants.DB.Contacts.TABLE_NAME + " ;";

    public int getNewContactID() {
        Cursor c = getReadableDatabase().rawQuery(MAX_ID_SQL, null);
        if (c.moveToNext())
            return c.getInt(c.getColumnIndex("ID")) + 1;
        return 0;
    }

}
