package com.sdkjordan.alkaff.androidapp112018.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

public class MyService extends Service {

    private static final String TAG =  "MyService";

    @Override
    public int onStartCommand(Intent data, int flags, int startId) {

        //  TODO: do you work here
        final int start = startId , flag = flags;
        final Intent intent = data ;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,String.format("%s-->%20s, flags:%10d, startId: %10d%n",Thread.currentThread().getName(),intent.getStringExtra(Constants.EXTRA_DATA),flag,start));

                try {
                    Thread.sleep(100000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stopSelf(start);
            }
        },"Thread "+startId).start();


        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //  TODO: return IBinder object if you want to allow service binding
        return null;
    }
}
