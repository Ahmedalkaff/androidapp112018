package com.sdkjordan.alkaff.androidapp112018;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

public class ReciverActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {


    Switch aSwitch;
    Button mButton;
    TextView mTextView, mTextViewbattery;
    EditText mEditText;

    MyReciver myReciver;
    IntentFilter mIntentFilter;
    boolean ReciverIsRegistered = false;

    BroadcastReceiver mBatteryLowReciver;
    IntentFilter batteryIntentFilter;
    Intent batteryStatus;


    private IntentFilter getBatteryIntentFilter() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        intentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        return intentFilter;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reciver);

        aSwitch = findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(this);
        mButton = findViewById(R.id.buttonNotification);
        mButton.setOnClickListener(this);
        mTextView = findViewById(R.id.textViewRecived);
        mEditText = findViewById(R.id.editText3);
        mTextViewbattery = findViewById(R.id.textViewBattery);
        myReciver = this.new MyReciver();
        mBatteryLowReciver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(getApplicationContext(), "batter is extremely low", Toast.LENGTH_SHORT).show();
            }
        };
        mIntentFilter = new IntentFilter(Constants.START_ACTION);
        batteryIntentFilter = getBatteryIntentFilter();


    }

    @Override
    protected void onResume() {
        super.onResume();


        if (mBatteryLowReciver != null)
            registerReceiver(mBatteryLowReciver, new IntentFilter(Intent.ACTION_BATTERY_LOW));
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mBatteryLowReciver != null)
            unregisterReceiver(mBatteryLowReciver);
    }

    @Override
    public void onClick(View v) {
        //Intent intent = new Intent(ReciverActivity.this,MyReciver.class);

        batteryStatus = registerReceiver(null, batteryIntentFilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float) scale;
        String stateString = "";
        switch (status) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                stateString = "CHARGING";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                stateString = "FULL";
                break;
            case BatteryManager.BATTERY_STATUS_UNKNOWN:
                stateString = "UNKNOWN";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                stateString = "NOT_CHARGING";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                stateString = "DISCHARGING";
                break;
        }

        mTextViewbattery.setText(stateString + ":Level:" + batteryPct + "%");


        Intent intent = new Intent(Constants.START_ACTION);
        intent.putExtra(Constants.EXTRA_DATA, mEditText.getText().toString());
        mEditText.setText("");
        sendBroadcast(intent);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            registerReceiver(myReciver, mIntentFilter);
        } else
            unregisterReceiver(myReciver);

        ReciverIsRegistered = isChecked;
    }


    public class MyReciver extends BroadcastReceiver {
        public MyReciver() {

        }

//        static  public  IntentFilter getIntentFilter()
//        {
//            return new IntentFilter();
//        }


        @Override
        public void onReceive(Context context, Intent intent) {

            Log.w(Constants.TAG, "MyReciver:onReceive()");
            if (intent != null) {
                String data = intent.getStringExtra(Constants.EXTRA_DATA);
                mTextView.setText(data);
            }

        }
    }


}
