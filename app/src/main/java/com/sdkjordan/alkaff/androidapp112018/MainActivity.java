package com.sdkjordan.alkaff.androidapp112018;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener ,View.OnLongClickListener {
    private static final String TAG = "MainActivity";
    Button button , button1, buttonGo;
    EditText mEditTextData;
    TextView mTextViewResult ;
    Random random;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate is called");
        setContentView(R.layout.activity_main);
        init();
    }


    private void init() {
        random = new Random();


        button = findViewById(R.id.button11);
        button1 = findViewById(R.id.button10);
        buttonGo = findViewById(R.id.buttonGo);

        mTextViewResult = findViewById(R.id.textView2);
        mEditTextData = findViewById(R.id.editTextData);
//        View.OnClickListener onClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId())
//                {
//                    case R.id.button10:
//                        firstActin(v);
//                        break;
//                    case R.id.button11:
//                        showToast("OnClick");
//                        break;
//                }
//            }
//        };
//
//        View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                switch (v.getId())
//                {
//                    case R.id.button10:
//                        showToast("onLongClick");
//                        break;
//                    case R.id.button11:
//
//                        firstActin(v);
//                        break;
//                }
//                return true;
//            }
//        } ;
        button.setOnClickListener(this);
        button1.setOnClickListener(this);

        buttonGo.setOnClickListener(this);
        button.setOnLongClickListener(this);
        button1.setOnLongClickListener(this);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                firstActin(v);
//            }
//        });
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showToast("OnClick");
//            }
//        });
//        button.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                firstActin(v);
//                return true;
//            }
//        });
//        button1.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                showToast("onLongClick");
//                return true;
//            }
//        });

    }

//    // For all buttons do the same action
//    public void onClick(View view) {
//        view.setBackgroundColor(random.nextInt());
//        if (view instanceof Button) {
//            button = (Button) view;
//            button.setText(button.getText().toString().equalsIgnoreCase("Yes") ? "No" : "Yes");
//        }
//    }

    // each button do different action
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.button10:
//                firstActin(view);
//                break;
//            case R.id.button11:
//                showToast("OnClick");
//                break;
//        }
//    }

    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    private void firstActin(View view) {
        view.setBackgroundColor(random.nextInt());
        if (view instanceof Button) {
            button = (Button) view;
            button.setText(button.getText().toString().equalsIgnoreCase("Yes") ? "No" : "Yes");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart is called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy is called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause is called");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume is called");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"onRestart is called");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button10:
                firstActin(v);
//                startActivity(new Intent(MainActivity.this,Main2Activity.class));
                Intent intent = new Intent(Constants.START_ACTION);
                startActivity(intent);
                break;
            case R.id.button11:
                //showToast("OnClick");
                startActivity(new Intent(MainActivity.this,NewActivity.class));
                break;
            case R.id.buttonGo:
                String text = mEditTextData.getText().toString();
                if(text != null && text.length() > 0)
                    goToActivity(text);
                break;
        }
    }

    private void goToActivity(String text) {
        Intent intent = new Intent(MainActivity.this,NewActivity.class);
        intent.putExtra(Constants.EXTRA_DATA,text);
        //startActivity(intent);
        startActivityForResult(intent,Constants.REQUEST_CODE1);
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId())
        {
            case R.id.button10:
                showToast("onLongClick");
                break;
            case R.id.button11:
                firstActin(v);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
       if(requestCode == Constants.REQUEST_CODE1)
       {
           switch (resultCode)
           {
               case RESULT_OK:
                   mTextViewResult.setText(data.getStringExtra(Constants.EXTRA_RESULT));
                   
                   break;
               case RESULT_CANCELED:
                   mTextViewResult.setText("The process was canceled!");
                   break ;
           }
       }
    }
}
