package com.sdkjordan.alkaff.androidapp112018;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;
import com.sdkjordan.alkaff.androidapp112018.services.MyIntentService;
import com.sdkjordan.alkaff.androidapp112018.services.MyService;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener {

    Button mStartServiceButton ;
    EditText mEditText ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        mStartServiceButton = findViewById(R.id.buttonStartService);
        mEditText = findViewById(R.id.editTextData);

        mStartServiceButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.buttonStartService:
                startTheService();
                break;
            case R.id.buttonToast:
                Toast.makeText(this,"I'm working!!",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void startTheService() {
        Intent intent = new Intent(ServiceActivity.this, MyService.class);
        intent.putExtra(Constants.EXTRA_DATA,mEditText.getText().toString());
        startService(intent);

        Intent intent1 = new Intent(ServiceActivity.this, MyIntentService.class);
        intent1.putExtra(Constants.EXTRA_DATA,mEditText.getText().toString());
        startService(intent1);
    }
}
