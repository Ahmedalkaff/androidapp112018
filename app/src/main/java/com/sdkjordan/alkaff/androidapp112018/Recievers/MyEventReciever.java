package com.sdkjordan.alkaff.androidapp112018.Recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.TAG;

public class MyEventReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null)
        {
            Log.e(TAG,intent.getStringExtra(Constants.EXTRA_DATA));
        }

    }
}
