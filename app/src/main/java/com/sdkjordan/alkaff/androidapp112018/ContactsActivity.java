package com.sdkjordan.alkaff.androidapp112018;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.sdkjordan.alkaff.androidapp112018.adapters.Contact;
import com.sdkjordan.alkaff.androidapp112018.adapters.MyAdapterWithHolder;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;
import com.sdkjordan.alkaff.androidapp112018.helpers.DatabaseHelper;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.TAG;

public class ContactsActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {


    ListView mListView;
    EditText mEditTextName, mEditTextPhone;
    //    SimpleAdapter mSimpleAdapter ;
    MyAdapterWithHolder myAdapter;
    SimpleCursorAdapter mSimpleCursorAdapter;

    DatabaseHelper mHelper;
    Random random = new Random();
    ContentValues values;
    private static final String[] from = {Constants.DB.Contacts.CONTACTS_NAME, Constants.DB.Contacts.CONTACTS_Phone};
    private static final int[] to = {R.id.textViewContactName, R.id.textViewPhoneNumber};
    private LinkedList<HashMap<String, String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        mHelper = new DatabaseHelper(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data = new LinkedList<>();
        myAdapter = new MyAdapterWithHolder(getApplicationContext());

        //mSimpleAdapter = new SimpleAdapter(getApplicationContext(),data,R.layout.contact_item_layout,from,to);
        mListView = findViewById(R.id.ListViewContacts);
//        mListView.setAdapter(mSimpleAdapter);

        Cursor c = mHelper.getUsers();

        mSimpleCursorAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.contact_item_layout, c, from, to, CursorAdapter.NO_SELECTION);


        mListView.setAdapter(mSimpleCursorAdapter);
        mListView.setOnItemLongClickListener(this);
        mListView.setOnItemClickListener(this);
        mEditTextName = findViewById(R.id.editTextName);
        mEditTextPhone = findViewById(R.id.editTextPhone);

        //TODO:load date

//       Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.a);
//        Bitmap.Config config = Bitmap.Config.ARGB_8888;
//        bitmap.reconfigure(200,200,config);
//       ImageView imageView ;
//       imageView.setImageBitmap(bitmap);

        values = new ContentValues();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mEditTextName.getText().toString();
                String phone = mEditTextPhone.getText().toString();
                if (name != null && name.length() > 3 && phone != null && phone.length() > 5) {

//                    int id = mHelper.getNewContactID();
//
//                    values.clear();
//                    values.put(Constants.DB.Contacts.CONTACTS_ID,id);
//                    values.put(Constants.DB.Contacts.CONTACTS_NAME,name);
//                    values.put(Constants.DB.Contacts.CONTACTS_Phone,phone);
//                    long rowID = mHelper.getWritableDatabase().insert(Constants.DB.Contacts.TABLE_NAME,null,values);

                    long rowID = mHelper.insertUser(name, phone);

                    if (rowID != -1) {
                        mEditTextName.setText("");
                        mEditTextPhone.setText("");

                        Cursor c = mHelper.getUsers();
                        mSimpleCursorAdapter.changeCursor(c);
                    } else {
                        Log.e(TAG, "Unable to save contact:Name:" + name + ", Phone:" + phone);
                        Toast.makeText(getApplicationContext(), "Unable to save contact:Name:" + name + ", Phone:" + phone, Toast.LENGTH_SHORT).show();
                    }
                    //  myAdapter.addContact(name, phone, 1 + random.nextInt(2));
//                    HashMap<String,String> contact = new HashMap<>();
//                    contact.put(Constants.NAME_KEY,name);
//                    contact.put(Constants.PHONE_KEY,phone);
//                    data.add(contact);
//                    mSimpleAdapter.notifyDataSetChanged();

                }

            }
        });
    }

//    private Cursor loadDataFromDB() {
//      return  mHelper.getReadableDatabase()
//                .query(Constants.DB.Contacts.TABLE_NAME,null,null,null,null,null,null) ;
//
//    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        //data.remove(position);
        //mSimpleAdapter.notifyDataSetChanged();

        myAdapter.removeContact(position);
        return true;
    }

    @SuppressLint("MissingPermission")
    private void callNumber(String num) {
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
        startActivity(callIntent);
    }

    private int position;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        this.position = position;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Constants.CALL_PERMISSION) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.CALL_PERMISSION)) {
                    Snackbar.make(view, "We need this permission to call", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                //requestPermissions();
                ActivityCompat.requestPermissions(this, new String[]{Constants.CALL_PERMISSION}, Constants.CALL_PREMESSION_REQ);
                return;


            }

            callNumber(myAdapter.getItem(position).getPhone());


//        Snackbar.make(view, data.get(position).get(Constants.NAME_KEY)+":"+ data.get(position).get(Constants.PHONE_KEY), Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == Constants.CALL_PREMESSION_REQ) {
            for (int i : grantResults)
                if (i != PackageManager.PERMISSION_GRANTED)
                    return;
        }
        callNumber(myAdapter.getItem(position).getPhone());
    }
}
