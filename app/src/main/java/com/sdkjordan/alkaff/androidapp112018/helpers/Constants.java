package com.sdkjordan.alkaff.androidapp112018.helpers;

import android.content.Intent;
import android.content.pm.PackageManager;

import java.security.Permission;
import java.security.Permissions;

public class Constants {
    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_RESULT = "result";
    public static final String IMAGE_KEY = "image" ;
    public static final String TAG = "androidapp112018" ;
    public static final String MAX_KEY = "MaxScore";
    public static final int NOTIFICATION_ID =  1;
    public static String NAME_KEY = "name";
    public static String PHONE_KEY = "phone";

    public  static String LINE_SEPARATOR = "\n";
//    static  {
//        LINE_SEPARATOR = System.getProperty("line.seprator");
//    }
    public static String FILE_NAME = "file.txt";

    public static final int CALL_PREMESSION_REQ = 15;
    public static final int STORAGE_PREMESSION_REQ = 20;


    public static final int REQUEST_CODE1 = 1 ;


    public static final String CALL_PERMISSION = "android.permission.CALL_PHONE";
    public static final String START_ACTION =  "com.sdkjordan.alkaff.action";

    public  static  class DB{
        public static final int VERSION = 2;
        public static final String DB_NAME= "contacts_db";

        public static  final class Contacts {
            public static final String TABLE_NAME= "contacts";
            public static final String CONTACTS_ID= "_id";
            public static final String CONTACTS_NAME= "Name";
            public static final String CONTACTS_Phone= "Phone";

        }

    }
}
