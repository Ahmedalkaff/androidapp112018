package com.sdkjordan.alkaff.androidapp112018;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.sdkjordan.alkaff.androidapp112018.helpers.Constants;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

import  static com.sdkjordan.alkaff.androidapp112018.helpers.Constants.TAG ;

public class DataManagementActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MAX_VALUR = 1000;
    private int max , ImageId;
    private String filename ;
    TextView mTextViewMax, mTextViewScore;
    EditText mEditTextDate;

    FloatingActionButton mFloatingActionButtonRandom, mFloatingActionButtonSave, mFloatingActionButtonRead;
    Random random = new Random();

    SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datamanagement);

        mPrefs = getPreferences(MODE_PRIVATE);      // this will call getSharedPreferences("DataManagementActivity",MODE_PRIVATE);
        // or
        //mPrefs = getSharedPreferences("myPrefs",MODE_PRIVATE);

        // TODO: Test push

        mEditTextDate = findViewById(R.id.editTextData);
        mTextViewMax = findViewById(R.id.textViewMax);
        mTextViewScore = findViewById(R.id.textViewScore);
        mFloatingActionButtonRandom = findViewById(R.id.floatingActionButton);
        mFloatingActionButtonSave = findViewById(R.id.floatingActionButton2);
        mFloatingActionButtonRead = findViewById(R.id.floatingActionButton3);
        mFloatingActionButtonRandom.setOnClickListener(this);
        mFloatingActionButtonSave.setOnClickListener(this);
        mFloatingActionButtonRead.setOnClickListener(this);
    }


    private int loadMax() {
        return mPrefs.getInt(Constants.MAX_KEY, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingActionButton:
                generateRandomNumber();
                break;
            case R.id.floatingActionButton2:
                saveTextToFileInternalStorage(mEditTextDate.getText().toString());
                mEditTextDate.setText("");
                break;
            case R.id.floatingActionButton3:
                readFileToText(Constants.FILE_NAME, mEditTextDate);
                filename = "ima1.png";
                ImageId = R.raw.avatar1 ;
                if(checkStoragepermission(v)) {

                    SaveFileToExternalStorage(ImageId, filename);
                }
        }

    }

    private boolean checkStoragepermission(View v) {

        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE ;
        if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,permission))
            {
                showScankBar("Write to external storage permission is needed to save file",v);
            }
            ActivityCompat.requestPermissions(this,new String[]{permission},Constants.STORAGE_PREMESSION_REQ);
            return  false ;
        }
        return  true ;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == Constants.STORAGE_PREMESSION_REQ)
        {
            for(int r : grantResults)
            {
                if(r != PackageManager.PERMISSION_GRANTED)
                    showScankBar("Sorrym we can't write to storage!",mFloatingActionButtonRandom);
                    return;
            }
            SaveFileToExternalStorage(ImageId, filename);
        }
    }

    private void showScankBar(String text, View v)
    {
        final Snackbar snackbar =  Snackbar.make(v, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    private void readFileToText(String fileName, EditText mEditTextDate) {
        FileInputStream fis = null;
        BufferedReader bufferedReader = null;
        StringBuilder buffer = new StringBuilder();
        try {
            fis = openFileInput(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(fis));
            String line ;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line ).append(Constants.LINE_SEPARATOR);

            }

            mEditTextDate.setText(buffer.toString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) bufferedReader.close();

                if (fis != null) fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveTextToFileInternalStorage(String s) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(Constants.FILE_NAME, MODE_APPEND);
            try (PrintWriter printWriter = new PrintWriter(fos)) {
                printWriter.println(s);
                // printWriter.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void SaveFileToExternalStorage(int imageId,String filename)
    {

        String state = Environment.getExternalStorageState();
        Log.i(TAG,"State:"+state);
        if(state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
        {
            File  pictureDirectory = getCacheDir() ;        // to save at cache directory (Internal storage and no permission is needed)
            pictureDirectory = getFilesDir() ;              // to save at files directory (Internal storage and no permission is needed)
           // File  pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
           Bitmap bmt =  BitmapFactory.decodeResource(getResources(),imageId);
           File target = new File(pictureDirectory,filename);
           try(FileOutputStream out = new FileOutputStream(target))
           {
               boolean result = bmt.compress(Bitmap.CompressFormat.PNG,90,out) ;
               if(!result )
                   Log.e(TAG,"Unable to svae the file:"+target.getAbsolutePath());
               else
                   Log.i(TAG,"Files is copied to :"+target.getAbsolutePath());
           } catch (IOException e) {
               Log.e(TAG,"Error:"+e.getStackTrace());
               e.printStackTrace();

           }finally {
               ImageId = -1 ;
               filename = null ;
           }

        }else {
            Log.e(TAG,"Media is not mounted state:"+state);
        }


    }
    private void generateRandomNumber() {
        int temp = random.nextInt(MAX_VALUR);
        mTextViewScore.setText(String.valueOf(temp));
        if (temp > max) {
            max = temp;
            mTextViewMax.setText(String.valueOf(max));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveMax(max);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTextViewMax.setText(String.valueOf(loadMax()));
    }

    private void saveMax(int max) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(Constants.MAX_KEY, max);
        editor.apply();
    }


}
