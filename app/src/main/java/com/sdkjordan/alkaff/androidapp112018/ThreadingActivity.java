package com.sdkjordan.alkaff.androidapp112018;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class ThreadingActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mImageView;
    private ProgressBar mpProgressBar;

    private Handler mMainUIThreadHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threading);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //
        mMainUIThreadHandler = new Handler();

        mpProgressBar = findViewById(R.id.progressBar);

        mImageView = findViewById(R.id.imageView2);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                break;
            case R.id.fabLoad:
                //loadImageNoThreading();
                //LoadImageWithThreading();
                loadImageUsingAsyncTask();
                break;
        }
    }

    private void loadImageNoThreading() {
        Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.android);
        // assume that loading image is time consuming e.g loading image from the internet with poor connection speed
        // to simulate that we will make sleep
        // this sleep will stop the main UI thread from taking the user interactions during the sleep period
        sleep(30 << 10);           // wait for a minute
        mImageView.setImageBitmap(img);

        // TODO: Never perform a long tasks in the main UI thread
    }


    // this to prevent the using from creating new thread each time a button is clicked
    private boolean isWorking = false;

    /**
     * Foe Android its not recommended to use the Thread class directly
     */
    private void loadImageWithThreading() {

        if (!isWorking) {
            final Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    isWorking = true;
                    mpProgressBar.setProgress(0);
                    final Bitmap img = BitmapFactory.decodeResource(getResources(), R.raw.android);
                    sleep(10 << 10);           // wait for 10 seconds
                    // here this will cause a CalledFromWrongThreadException, becuase only the main thread can change on the mImageView
                    //mImageView.setImageBitmap(img);

                    // to solve this you have some options :
                    // option 1: use View.post method to send this back to main UI thread
                    mImageView.post(new Runnable() {
                        @Override
                        public void run() {
                            mImageView.setImageBitmap(img);
                        }
                    });

                    // option 2: use runOnUiThread to execute the code in the main UI thread
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mImageView.setImageBitmap(img);
//                        }
//                    });

                    // option 3: use main Ui Thread Handler to post the code back to the main UI thread
//                    mMainUIThreadHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mImageView.setImageBitmap(img);
//                        }
//                    });

                    mpProgressBar.setProgress(0);
                    isWorking = false;
                }
            });
            t.start();


        }

    }

    private void loadImageUsingAsyncTask()
    {
        if(!isWorking)
            new ImageLoader(getApplicationContext()).execute(R.raw.android,10<<10);
    }

    // When an object of this class is create and execute method is called then a thread will be created
    // in which the code inside doInBackground method will be executed
    private class ImageLoader extends AsyncTask<Integer, Float, Bitmap> {

        private Context mContext ;
        public ImageLoader(Context context) {
            mContext = context ;
        }

        @Override
        protected void onPreExecute() {         // run on Main UI before starting doInBackground
            super.onPreExecute();
            mpProgressBar.setProgress(0);
            isWorking = true ;

        }

        @Override
        protected void onProgressUpdate(Float... values) {    // run on Main UI when  publishProgress is called from the doInBackground
            super.onProgressUpdate(values);
            mpProgressBar.setProgress((int) (mpProgressBar.getMax() * values[0]));
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {   // run on Main UI  doInBackground is canceled after completion
            super.onCancelled(bitmap);
            mImageView.setImageBitmap(bitmap);
        }

        @Override
        protected void onCancelled() {                  // run on Main UI if the doInBackground is canceled before completion
            super.onCancelled();
            mImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),R.raw.avatar1));
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {              // Run at Worker thread

            if (integers == null) {
                cancel(true);
                return null;
            }

            int delay;
            if (integers.length == 1)
                delay = 10 << 10;
            else
                delay = integers[1];

            Bitmap img = BitmapFactory.decodeResource(mContext.getResources(), integers[0]);
            int period = delay / 100;
            try {
                for (int i = 1; i <= 100; i++) {
                    publishProgress((float) ( i / 100.0F));
                    Thread.sleep(period);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return img;
        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {   // run on Main UI after starting doInBackground
            super.onPostExecute(bitmap);
            mImageView.setImageBitmap(bitmap);
            isWorking = false ;
        }

    }

    private void sleep(int delay) {

        int period = delay / 100;
        try {
            for (int i = 0; i < 100; i++) {
                mpProgressBar.setProgress(i);
                Thread.sleep(period);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
